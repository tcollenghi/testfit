﻿
namespace FitTest
{
    partial class FrmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPalíndromo = new MaterialSkin.Controls.MaterialLabel();
            this.txtPalindromo = new System.Windows.Forms.TextBox();
            this.lblNumeroPrimo = new MaterialSkin.Controls.MaterialLabel();
            this.txtNumeroPrimo = new System.Windows.Forms.TextBox();
            this.btnNumeroPrimo = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialRaisedButton1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.btnSair = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.txtInicioX = new System.Windows.Forms.TextBox();
            this.txtInicioY = new System.Windows.Forms.TextBox();
            this.txtFimX = new System.Windows.Forms.TextBox();
            this.txtFimY = new System.Windows.Forms.TextBox();
            this.btnVerificarDeslocamento = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialDivider1 = new MaterialSkin.Controls.MaterialDivider();
            this.materialDivider2 = new MaterialSkin.Controls.MaterialDivider();
            this.materialDivider3 = new MaterialSkin.Controls.MaterialDivider();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.materialRaisedButton2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.SuspendLayout();
            // 
            // lblPalíndromo
            // 
            this.lblPalíndromo.AutoSize = true;
            this.lblPalíndromo.Depth = 0;
            this.lblPalíndromo.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblPalíndromo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblPalíndromo.Location = new System.Drawing.Point(25, 161);
            this.lblPalíndromo.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblPalíndromo.Name = "lblPalíndromo";
            this.lblPalíndromo.Size = new System.Drawing.Size(86, 19);
            this.lblPalíndromo.TabIndex = 0;
            this.lblPalíndromo.Text = "Palíndromo";
            // 
            // txtPalindromo
            // 
            this.txtPalindromo.Location = new System.Drawing.Point(29, 214);
            this.txtPalindromo.Name = "txtPalindromo";
            this.txtPalindromo.Size = new System.Drawing.Size(529, 20);
            this.txtPalindromo.TabIndex = 1;
            this.txtPalindromo.Text = "level";
            // 
            // lblNumeroPrimo
            // 
            this.lblNumeroPrimo.AutoSize = true;
            this.lblNumeroPrimo.Depth = 0;
            this.lblNumeroPrimo.Font = new System.Drawing.Font("Roboto", 11F);
            this.lblNumeroPrimo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblNumeroPrimo.Location = new System.Drawing.Point(25, 77);
            this.lblNumeroPrimo.MouseState = MaterialSkin.MouseState.HOVER;
            this.lblNumeroPrimo.Name = "lblNumeroPrimo";
            this.lblNumeroPrimo.Size = new System.Drawing.Size(107, 19);
            this.lblNumeroPrimo.TabIndex = 2;
            this.lblNumeroPrimo.Text = "Número Primo";
            // 
            // txtNumeroPrimo
            // 
            this.txtNumeroPrimo.Location = new System.Drawing.Point(29, 112);
            this.txtNumeroPrimo.Name = "txtNumeroPrimo";
            this.txtNumeroPrimo.Size = new System.Drawing.Size(226, 20);
            this.txtNumeroPrimo.TabIndex = 3;
            // 
            // btnNumeroPrimo
            // 
            this.btnNumeroPrimo.AutoSize = true;
            this.btnNumeroPrimo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnNumeroPrimo.Depth = 0;
            this.btnNumeroPrimo.Icon = null;
            this.btnNumeroPrimo.Location = new System.Drawing.Point(277, 96);
            this.btnNumeroPrimo.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnNumeroPrimo.Name = "btnNumeroPrimo";
            this.btnNumeroPrimo.Primary = true;
            this.btnNumeroPrimo.Size = new System.Drawing.Size(238, 36);
            this.btnNumeroPrimo.TabIndex = 4;
            this.btnNumeroPrimo.Text = "Verificar se o número é Primo";
            this.btnNumeroPrimo.UseVisualStyleBackColor = true;
            this.btnNumeroPrimo.Click += new System.EventHandler(this.btnNumeroPrimo_Click);
            // 
            // materialRaisedButton1
            // 
            this.materialRaisedButton1.AutoSize = true;
            this.materialRaisedButton1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton1.Depth = 0;
            this.materialRaisedButton1.Icon = null;
            this.materialRaisedButton1.Location = new System.Drawing.Point(577, 198);
            this.materialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton1.Name = "materialRaisedButton1";
            this.materialRaisedButton1.Primary = true;
            this.materialRaisedButton1.Size = new System.Drawing.Size(293, 36);
            this.materialRaisedButton1.TabIndex = 5;
            this.materialRaisedButton1.Text = "Verificar se o texto é um palíndromo";
            this.materialRaisedButton1.UseVisualStyleBackColor = true;
            this.materialRaisedButton1.Click += new System.EventHandler(this.materialRaisedButton1_Click);
            // 
            // btnSair
            // 
            this.btnSair.AutoSize = true;
            this.btnSair.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnSair.Depth = 0;
            this.btnSair.Icon = null;
            this.btnSair.Location = new System.Drawing.Point(819, 615);
            this.btnSair.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnSair.Name = "btnSair";
            this.btnSair.Primary = true;
            this.btnSair.Size = new System.Drawing.Size(51, 36);
            this.btnSair.TabIndex = 6;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(25, 262);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(37, 19);
            this.materialLabel1.TabIndex = 7;
            this.materialLabel1.Text = "BOT";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(25, 306);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(108, 19);
            this.materialLabel2.TabIndex = 8;
            this.materialLabel2.Text = "Posição Inicial";
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(284, 306);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(100, 19);
            this.materialLabel3.TabIndex = 9;
            this.materialLabel3.Text = "Posição Final";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(25, 346);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(103, 19);
            this.materialLabel4.TabIndex = 10;
            this.materialLabel4.Text = "Coordenada X";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(25, 388);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(103, 19);
            this.materialLabel5.TabIndex = 11;
            this.materialLabel5.Text = "Coordenada Y";
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(284, 346);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(103, 19);
            this.materialLabel6.TabIndex = 12;
            this.materialLabel6.Text = "Coordenada X";
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(284, 388);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(103, 19);
            this.materialLabel7.TabIndex = 13;
            this.materialLabel7.Text = "Coordenada Y";
            // 
            // txtInicioX
            // 
            this.txtInicioX.Location = new System.Drawing.Point(151, 345);
            this.txtInicioX.Name = "txtInicioX";
            this.txtInicioX.Size = new System.Drawing.Size(84, 20);
            this.txtInicioX.TabIndex = 14;
            this.txtInicioX.Text = "1";
            // 
            // txtInicioY
            // 
            this.txtInicioY.Location = new System.Drawing.Point(151, 389);
            this.txtInicioY.Name = "txtInicioY";
            this.txtInicioY.Size = new System.Drawing.Size(84, 20);
            this.txtInicioY.TabIndex = 15;
            this.txtInicioY.Text = "1";
            // 
            // txtFimX
            // 
            this.txtFimX.Location = new System.Drawing.Point(412, 345);
            this.txtFimX.Name = "txtFimX";
            this.txtFimX.Size = new System.Drawing.Size(84, 20);
            this.txtFimX.TabIndex = 16;
            // 
            // txtFimY
            // 
            this.txtFimY.Location = new System.Drawing.Point(412, 387);
            this.txtFimY.Name = "txtFimY";
            this.txtFimY.Size = new System.Drawing.Size(84, 20);
            this.txtFimY.TabIndex = 17;
            // 
            // btnVerificarDeslocamento
            // 
            this.btnVerificarDeslocamento.AutoSize = true;
            this.btnVerificarDeslocamento.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnVerificarDeslocamento.Depth = 0;
            this.btnVerificarDeslocamento.Icon = null;
            this.btnVerificarDeslocamento.Location = new System.Drawing.Point(561, 298);
            this.btnVerificarDeslocamento.MouseState = MaterialSkin.MouseState.HOVER;
            this.btnVerificarDeslocamento.Name = "btnVerificarDeslocamento";
            this.btnVerificarDeslocamento.Primary = true;
            this.btnVerificarDeslocamento.Size = new System.Drawing.Size(309, 36);
            this.btnVerificarDeslocamento.TabIndex = 18;
            this.btnVerificarDeslocamento.Text = "Verificar se é possível o deslocamento";
            this.btnVerificarDeslocamento.UseVisualStyleBackColor = true;
            this.btnVerificarDeslocamento.Click += new System.EventHandler(this.materialRaisedButton2_Click);
            // 
            // materialDivider1
            // 
            this.materialDivider1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider1.Depth = 0;
            this.materialDivider1.Location = new System.Drawing.Point(29, 138);
            this.materialDivider1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider1.Name = "materialDivider1";
            this.materialDivider1.Size = new System.Drawing.Size(846, 10);
            this.materialDivider1.TabIndex = 19;
            this.materialDivider1.Text = "materialDivider1";
            // 
            // materialDivider2
            // 
            this.materialDivider2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider2.Depth = 0;
            this.materialDivider2.Location = new System.Drawing.Point(29, 240);
            this.materialDivider2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider2.Name = "materialDivider2";
            this.materialDivider2.Size = new System.Drawing.Size(846, 10);
            this.materialDivider2.TabIndex = 20;
            this.materialDivider2.Text = "materialDivider2";
            // 
            // materialDivider3
            // 
            this.materialDivider3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialDivider3.Depth = 0;
            this.materialDivider3.Location = new System.Drawing.Point(24, 415);
            this.materialDivider3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialDivider3.Name = "materialDivider3";
            this.materialDivider3.Size = new System.Drawing.Size(846, 10);
            this.materialDivider3.TabIndex = 21;
            this.materialDivider3.Text = "materialDivider3";
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(25, 440);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(59, 19);
            this.materialLabel8.TabIndex = 22;
            this.materialLabel8.Text = "Divisão";
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(44, 479);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(77, 19);
            this.materialLabel9.TabIndex = 23;
            this.materialLabel9.Text = "dividendo ";
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(44, 517);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(58, 19);
            this.materialLabel10.TabIndex = 24;
            this.materialLabel10.Text = "divisor ";
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(44, 560);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(80, 19);
            this.materialLabel11.TabIndex = 25;
            this.materialLabel11.Text = "quociente ";
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(44, 601);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(48, 19);
            this.materialLabel12.TabIndex = 26;
            this.materialLabel12.Text = "resto ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(151, 478);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(84, 20);
            this.textBox1.TabIndex = 27;
            this.textBox1.Text = "1";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(151, 516);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(84, 20);
            this.textBox2.TabIndex = 28;
            this.textBox2.Text = "1";
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(151, 559);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(84, 20);
            this.textBox3.TabIndex = 29;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(151, 602);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(84, 20);
            this.textBox4.TabIndex = 30;
            // 
            // materialRaisedButton2
            // 
            this.materialRaisedButton2.AutoSize = true;
            this.materialRaisedButton2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.materialRaisedButton2.Depth = 0;
            this.materialRaisedButton2.Icon = null;
            this.materialRaisedButton2.Location = new System.Drawing.Point(561, 462);
            this.materialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialRaisedButton2.Name = "materialRaisedButton2";
            this.materialRaisedButton2.Primary = true;
            this.materialRaisedButton2.Size = new System.Drawing.Size(145, 36);
            this.materialRaisedButton2.TabIndex = 31;
            this.materialRaisedButton2.Text = "Executar Divisão";
            this.materialRaisedButton2.UseVisualStyleBackColor = true;
            this.materialRaisedButton2.Click += new System.EventHandler(this.materialRaisedButton2_Click_1);
            // 
            // FrmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(887, 674);
            this.Controls.Add(this.materialRaisedButton2);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.materialLabel12);
            this.Controls.Add(this.materialLabel11);
            this.Controls.Add(this.materialLabel10);
            this.Controls.Add(this.materialLabel9);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.materialDivider3);
            this.Controls.Add(this.materialDivider2);
            this.Controls.Add(this.materialDivider1);
            this.Controls.Add(this.btnVerificarDeslocamento);
            this.Controls.Add(this.txtFimY);
            this.Controls.Add(this.txtFimX);
            this.Controls.Add(this.txtInicioY);
            this.Controls.Add(this.txtInicioX);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.materialRaisedButton1);
            this.Controls.Add(this.btnNumeroPrimo);
            this.Controls.Add(this.txtNumeroPrimo);
            this.Controls.Add(this.lblNumeroPrimo);
            this.Controls.Add(this.txtPalindromo);
            this.Controls.Add(this.lblPalíndromo);
            this.Name = "FrmPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Testes Práticos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel lblPalíndromo;
        private System.Windows.Forms.TextBox txtPalindromo;
        private MaterialSkin.Controls.MaterialLabel lblNumeroPrimo;
        private System.Windows.Forms.TextBox txtNumeroPrimo;
        private MaterialSkin.Controls.MaterialRaisedButton btnNumeroPrimo;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton1;
        private MaterialSkin.Controls.MaterialRaisedButton btnSair;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private System.Windows.Forms.TextBox txtInicioX;
        private System.Windows.Forms.TextBox txtInicioY;
        private System.Windows.Forms.TextBox txtFimX;
        private System.Windows.Forms.TextBox txtFimY;
        private MaterialSkin.Controls.MaterialRaisedButton btnVerificarDeslocamento;
        private MaterialSkin.Controls.MaterialDivider materialDivider1;
        private MaterialSkin.Controls.MaterialDivider materialDivider2;
        private MaterialSkin.Controls.MaterialDivider materialDivider3;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private MaterialSkin.Controls.MaterialRaisedButton materialRaisedButton2;
    }
}

