﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Configuration;
using System.IO;

using MaterialSkin;
using MaterialSkin.Controls;
namespace FitTest
{
    public partial class FrmPrincipal :  MaterialForm
    {
        private readonly MaterialSkinManager materialSkinManager;
        public FrmPrincipal()
        {
            InitializeComponent();
            materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue400, Primary.Blue500, Primary.Blue600, Accent.Blue700, TextShade.WHITE);
        }

        private void btnNumeroPrimo_Click(object sender, EventArgs e)
        {
            int numero ;
            //verifica se o campo txtNumeroPrimo está vazio 
            if (txtNumeroPrimo.Text == "" || !int.TryParse(txtNumeroPrimo.Text, out numero))
            {
                MessageBox.Show("Digite um número para verificar se é primo", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNumeroPrimo.Focus();
            }
            else
            {
                var iteracoes = VerificaPrimo(numero);
                //verifica se o número digitado é primo
                if (iteracoes == 0 )
                {
                    MessageBox.Show("O número " + txtNumeroPrimo.Text + " não é primo", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("O número " + txtNumeroPrimo.Text + " é primo, Número de iterações necessárias: " + iteracoes.ToString(), 
                                    "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    
                }
            }
        }

        public static bool EhPalindromo(string input)
        {
            // Remove qq caractere que não seja letra ou número e converte para minúsculo
            string cleanedInput = new string(input.Where(char.IsLetterOrDigit).Select(char.ToLower).ToArray());

            // Verifica se a string é igual a ela mesma invertida
            return cleanedInput.SequenceEqual(cleanedInput.Reverse());
        }

        private int VerificaPrimo(int numero)
        {
            // se numero for 1 ou 0 não é primo
            if (numero == 1 || numero == 0)
            {
                return 0;
            }
            else
            {
                //verifica se o número é divisível por outro número
                int count = 1;
                for (int i = 2; i < numero; i++)
                {
                    
                    //se o número for divisível por outro número não é primo
                    if ((numero % i == 0) && (numero % count == 0) || (numero % 3 == 0))
                    {
                        return 0;
                    }
                    else
                    {
                        count++;
                    }
                }
                return count;
            }
        }

        private void materialRaisedButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtPalindromo.Text))
            {
                MessageBox.Show("Digite uma palavra para verificar se é um palíndromo", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtPalindromo.Focus();
                return;
            }

            if (EhPalindromo(this.txtPalindromo.Text))
            {
                MessageBox.Show("A palavra " + this.txtPalindromo.Text + " é um palíndromo", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("A palavra " + this.txtPalindromo.Text + " não é um palíndromo", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void materialRaisedButton2_Click(object sender, EventArgs e)
        {
            // verifica se as coordenadas final x esta vazia
            if (string.IsNullOrEmpty(this.txtFimX.Text))
            {
                MessageBox.Show("Digite as coordenadas finais do robô", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtFimX.Focus();
                return;
            }

            // verifica se as coordenadas final y esta vazia
            if (string.IsNullOrEmpty(this.txtFimY.Text))
            {
                MessageBox.Show("Digite as coordenadas finais do robô", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.txtFimY.Focus();
                return;
            }

            // faz o deslocamento do robô
            Bot bot = new Bot(int.Parse(this.txtInicioX.Text), int.Parse(this.txtInicioY.Text));

            //verifica se o robo pode chegar na coordenada final x 
            while (bot.X < int.Parse(this.txtFimX.Text))
            {
                bot.X = bot.X + int.Parse(this.txtInicioY.Text);
            }

            //se a coordenada inicial x somado ao deslocamento do robô for maior que a coordenada final x
            //dispara uma mensagem de erro
            if (bot.X > int.Parse(this.txtFimX.Text))
            {
                MessageBox.Show("O robô não pode chegar na coordenada final X", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            //verifica se o robo pode chegar na coordenada final y
            while (bot.Y < int.Parse(this.txtFimY.Text))
            {
                bot.Y = bot.Y + int.Parse(this.txtInicioX.Text);
            }

            //se a coordenada inicial y somado ao deslocamento do robô for maior que a coordenada final y
            //dispara uma mensagem de erro
            if (bot.Y > int.Parse(this.txtFimY.Text))
            {
                MessageBox.Show("O robô não pode chegar na coordenada final Y", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // se o robô chegar nas coordenadas finais x e y dispara uma mensagem de sucesso
            MessageBox.Show("O robô chegou nas coordenadas finais X e Y", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
            

        }

        private void materialRaisedButton2_Click_1(object sender, EventArgs e)
        {
            int dividendo = 10;
            int divisor = 2;

            int resultado = Divide(dividendo, divisor);

            MessageBox.Show($"O resultado da divisão é: {resultado} " , "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Console.WriteLine();
        }

        static int Divide(int dividendo, int divisor)
        {
            if (divisor == 0)
            {
                throw new ArgumentException("O divisor não pode ser zero.");
            }

            int contador = 0;
            while (dividendo >= divisor)
            {
                dividendo -= divisor;
                contador++;
            }

            return contador;
        }
    }
}
