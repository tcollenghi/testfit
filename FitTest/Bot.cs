﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitTest
{
    public class Bot
    {

        public int X { get; set; }
        public int Y { get; set; }

        //construtor com coordenadas iniciais
        public Bot(int x, int y)
        {
            X = x;
            Y = y;
        }

        public enum Direcao
        {
            Acima, Direita
        }

        public void Mover(Direcao direcao)
        {
            switch (direcao)
            {
                case Direcao.Acima:
                    X = X + Y;
                    break;
                case Direcao.Direita:
                    Y = X + Y;
                    break;
            }
        }



    }
}
